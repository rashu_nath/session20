<?php

namespace App\Model;
use PDO, PDOException;

class Database
{
  public $DBH;

  public function __construct()
  {
      if(!isset($_SESSION)){
          session_start();
      }


      try {
           $this->DBH = new PDO('mysql:host=localhost;dbname=trainer_atomic_project_b46','root', '');


      } catch (PDOException $error) {
        echo "Hello Error!: " . $error->getMessage(). "<br/>";
        die();
      }



  }

}