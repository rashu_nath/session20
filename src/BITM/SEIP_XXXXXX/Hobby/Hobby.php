<?php
namespace App\Hobby;

use App\Message\Message;
use App\Utility\Utility;

use App\Model\Database as DB;

class Hobby extends DB
{

    private $id;
    private $name;
    private $hobby;
    private $soft_deleted;


    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("name",$postData)){
            $this->name = $postData["name"];
        }

        if(array_key_exists("hobby",$postData)){
            $this->hobby = $postData["hobby"];
        }

        if(array_key_exists("soft_deleted",$postData)){
            $this->soft_deleted = $postData["soft_deleted"];
        }
    }


    public function store(){

        $dataArray = array($this->name,$this->hobby) ;


        $sql = "insert into hobby(name,hobby) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =  $STH->execute($dataArray);


        if($result){

            Message::message("Success! :) Data Has Been Inserted!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Inserted!<br>")  ;

        }


        Utility::redirect('create.php');


    }






}